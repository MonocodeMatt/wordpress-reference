<?php

/**
 * Boilerplate for all default capabilities
 * https://wordpress.org/support/article/roles-and-capabilities/#create_sites
 */

$default_capabilities = [

    // -- Multisites --
        // Sites
            'create_sites'              => false, // Allows user to create sites on the network
            'delete_site'               => false, // Allows user to delete the current site
            'delete_sites'              => false, // Allows user to delete sites on the network
            'manage_sites'              => false, // Allows user to add, delete, archive, unarchive, activate, deactivate, spam, unspam new site/blog in the network

        // Network
            'manage_network'            => false, // Allows user to upgrade network
            'manage_network_users'      => false, // Allows access to Network Users menu
            'manage_network_plugins'    => false, // Allows access to Network Themes menu
            'manage_network_themes'     => false, // Allows access to Network Options menu
            'manage_network_options'    => false, // Allows access to Network Plugins menu
            'upgrade_network'           => false, // Determines access to network upgrade page/display network upgrade notice
            'setup_network'             => false, // Determines wether an individual can convert site over to multisite.
    
    // -- Post Types --
        // Links
            'manage_links'              => false, // Allows access to Links & Links > Add New

        // Pages
            'delete_others_pages'       => false, // Grants capability to delete another author's pages
            'delete_pages'              => false, // Grants capability to delete page
            'delete_private_pages'      => false, // Grants capability to delete pages that are set to private
            'delete_published_pages'    => false, // Grants capability to delete pages that have already been published
            'edit_others_pages'         => false, // Grants access to pages authored by another user, drafts, & moderation of comments
            'edit_pages'                => false, // Grants access to Pages, Pages > Add New, Comments, Comments > Awaiting Moderation
            'edit_private_pages'        => false, // Grants capability to edit a page set to private
            'edit_published_pages'      => false, // Off by default (must be applied on demand), prevents users from being able to modify an already published page
            'publish_pages'             => false, // Grants capability to publish page (otherwise can only save drafts)
            'read_private_pages'        => false, // Grants capability to read pages set to private
        
        // Posts
            'delete_others_posts'       => false, // Grants capability to delete another author's posts
            'delete_posts'              => false, // Grants capability to delete post
            'delete_private_posts'      => false, // Grants capability to delete posts that are set to private
            'delete_published_posts'    => false, // Grants capability to delete posts that have already been published
            'edit_others_posts'         => false, // Grants access to posts authored by another user, drafts, & moderation of comments
            'edit_posts'                => false, // Grants access to Posts, Posts > Add New, Comments, Comments > Awaiting Moderation
            'edit_private_posts'        => false, // Grants capability to edit a post set to private
            'edit_published_posts'      => false, // Off by default (must be applied on demand), prevents users from being able to modify an already published post
            'publish_posts'             => false, // Grants capability to publish post (otherwise can only save drafts)
            'read_private_posts'        => false, // Grants capability to read posts set to private

        // Media
            'edit_files'                => false, // DEPRECATED
            'upload_files'              => false, // Allows access to Media & Media > Add New
            'unfiltered_upload'         => false, // ???

        // Comments
            'edit_comment'              => false, // Allows modification of comment?
            'moderate_comments'         => false, // In conjunction with "edit_posts" capability, allows user to moderate comments

    // -- Taxonomies --
        // Categories
            'manage_categories'         => false, // Allows access to Posts > Categories & Links > Categories
    
    // -- Admin Dashboard --
        // Dashboard
            'edit_dashboard'            => false, // Grants capability to edit own admin dashboard

        // Plugins
            'activate_plugins'          => false, // Allows acces to Plugin options
            'delete_plugins'            => false, // Grants capability to delete plugins
            'edit_plugins'              => false, // Allows access to Plugins > Plugin Editor
            'install_plugins'           => false, // Allows access to Plugins > Add New
            'update_plugins'            => false, // Grants capability to update plugins
    
        // Theme
            'edit_theme_options'        => false, // Allows access to appearance > Widgets | Menus | Customize | Header pages
            'customize'                 => false, // Allows access to customizer
            'edit_themes'               => false, // Allows access to appearance > theme editor page
            'delete_themes'             => false, // Grants capability to delete a theme
            'install_themes'            => false, // Allows access to appearance > Add New Themes page
            'switch_themes'             => false, // Allows access to appearance and theme pages
            'update_themes'             => false, // Grants capability to update theme(s)
    
        // Editor
            'unfiltered_html'           => false, // Allows user to post raw markup/JS in posts (and other various post types)

        // Tools
            'export'                    => false, // Grants Capability to export???
            'import'                    => false, // Allows access to Tools > Import | Export
    
        // Users
            'add_users'                 => false, // DEPRECATED - See promote_users
            'create_users'              => false, // Grants capability to create a new user account (unless other capabilities are provided, blog will assign the default role)
            'delete_users'              => false, // Grants capability to delete user account???
            'edit_users'                => false, // Allows access to Users page
            'list_users'                => false, // Allows access to Users page
            'promote_users'             => false, // Allows access to controls for changing a user's role & "Add Existing User" for multisite installations
            'remove_users'              => false, // Grants capability to delete user account???
    
        // Options
            'manage_options'            => false, // Allows access to Settings > General | Writing | Reading | Discussion | Permalinks | Miscellaneous pages

        // Core
            'update_core'               => false, // Grants capability to upgrade WordPress

        // Read
            'read'                      => false, // Used for determining if the user can access dashboard as well as their profile.
    
];